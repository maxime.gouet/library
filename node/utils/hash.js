'use strict'

const crypto = require('crypto')
const algo = 'aes-256-cbc'
const secret = process.env['BIBLIO_SECRET'] || 'w9z$C&F)J@NcRfTjWnZr4u7x!A%D*G-K'

const encrypt = function (str) {
    const iv = crypto.randomBytes(16)
    const cipher = crypto.createCipheriv(algo, secret, iv)
    const encrypted = Buffer.concat([cipher.update(str), cipher.final()]).toString('hex')

    return JSON.stringify({
        iv: iv.toString("hex"),
        content: encrypted.toString('hex')
    })
}

const decrypt = function (hash) {
    hash = JSON.parse(hash)
    const decipher = crypto.createDecipheriv(algo, secret, Buffer.from(hash.iv, 'hex'));
    const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);
    return decrpyted.toString();
}

exports.encrypt = encrypt
exports.decrypt = decrypt