package fr.ensicaen.ws_bibl.controller;

import fr.ensicaen.ws_bibl.entity.BookEntity;
import fr.ensicaen.ws_bibl.entity.BorrowingEntity;
import fr.ensicaen.ws_bibl.entity.PublisherEntity;
import fr.ensicaen.ws_bibl.exception.WrongIDException;
import fr.ensicaen.ws_bibl.service.AuthorService;
import fr.ensicaen.ws_bibl.service.BookService;
import fr.ensicaen.ws_bibl.service.PublisherService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("book")
public class BookController {
    private final BookService bookService;
    private final AuthorService authorService;
    private final PublisherService publisherService;

    public BookController(BookService bookService, AuthorService authorService, PublisherService publisherService) {
        this.bookService = bookService;
        this.authorService = authorService;
        this.publisherService = publisherService;
    }

    @GetMapping(value = "/", produces = "application/json")
    public Collection<BookEntity> index(@RequestParam(required = false) String name) {
        if (name != null) {
            return bookService.getByName(name);
        } else {
            return bookService.list();
        }
    }

    @PostMapping("/")
    public ResponseEntity<BookEntity> add(@RequestBody BookEntity bookEntity) throws WrongIDException {
        Optional<PublisherEntity> publisher = publisherService.getById(bookEntity.getPublisher_id().toString());

        if(!publisher.isPresent()) throw WrongIDException.builder(String.valueOf(bookEntity.getPublisher_id()), BorrowingEntity.class);

        BookEntity newBook = bookService.add(bookEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(newBook);
    }

    @GetMapping("{id}")
    public ResponseEntity<Object> getById(@PathVariable String id) throws WrongIDException {
        Optional<BookEntity> bookEntity = bookService.getById(id);
        if (bookEntity.isEmpty()) {
            return notfound(id);
        } else {
            return ResponseEntity.ok(bookEntity.get());
        }
    }

    private ResponseEntity<Object> notfound(String id) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("<html><body>cannot found BookEntity with id=" + id + "</body></html>");
    }
}