const url = "http://localhost:8080/books"
const publisher_url = "http://localhost:8080/publishers/"

axios.get(url)
    .then((response) => {
        if ('data' in response) {
            let tbody = document.getElementsByTagName("tbody")[0]
            response.data.forEach((book) => {
                axios.get(publisher_url + book.publisher_id)
                    .then((response) => {
                        if ('data' in response) {
                            let toInsert = `
                                <td>${book.id}</td>
                                <td>${book.name}</td>
                                <td>${book.status}</td>
                                <td>${book.publish_date}</td>
                                <td>${response.data.name}</td>`
                            let tr = document.createElement("tr")
                            tr.innerHTML = toInsert
                            tbody.appendChild(tr);
                        }
                    })
                    .catch((err) => {
                        document.getElementById("error").innerHTML = err
                    })
            })
        }
    })
    .catch((err) => {
        document.getElementById("error").innerHTML = err
    })