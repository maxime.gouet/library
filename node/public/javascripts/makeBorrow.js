const borrow_url = "http://localhost:8080/borrowings/"
const users_url = "http://localhost:8080/users/"
const books_url = "http://localhost:8080/books/"

axios.get(users_url)
    .then((response) => {
        if ('data' in response) {
            let select = document.getElementById("user")
            response.data.forEach((user) => {
                let option = document.createElement("option")
                option.innerHTML = `${user.name} - ${user.role}`
                option.value = user.id_user
                select.appendChild(option)
            })
        }
    })
    .catch((err) => {
        document.getElementById("error").innerHTML = err
    })

axios.get(books_url)
    .then((response) => {
        if ('data' in response) {
            let select = document.getElementById("book")
            response.data.forEach((book) => {
                let option = document.createElement("option")
                option.innerHTML = `${book.name}`
                option.value = book.id
                select.appendChild(option)
            })
        }
    })
    .catch((err) => {
        document.getElementById("error").innerHTML = err
    })

document.getElementsByTagName("button")[0].onclick = function() {
    let duree = document.getElementById("duree").value
    let user = document.getElementById("user").value
    let book = document.getElementById("book").value

    axios({
        method: 'post',
        url: borrow_url,
        data: {
            borrow_duration: duree,
            book_id: book,
            id_user: user,
        }
    })
        .then(() => {
            document.getElementById("ok").innerHTML = "Emprunt ajouté avec succès"
            document.getElementById("error").innerHTML = ""
        })
        .catch((err) => {
            document.getElementById("ok").innerHTML = ""
            document.getElementById("error").innerHTML = err
        })
}
