package fr.ensicaen.ws_bibl.service;

import fr.ensicaen.ws_bibl.entity.BookEntity;
import fr.ensicaen.ws_bibl.entity.BorrowingEntity;
import fr.ensicaen.ws_bibl.exception.WrongIDException;
import fr.ensicaen.ws_bibl.repository.BookRepository;
import fr.ensicaen.ws_bibl.repository.BorrowingRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
public class BorrowingService {
    private final BorrowingRepository borrowingRepository;
    private final BookRepository bookRepository;

    public BorrowingService(BorrowingRepository borrowingRepository,BookRepository bookRepository) {
        this.borrowingRepository = borrowingRepository;
        this.bookRepository = bookRepository;
    }

    public BorrowingEntity add(BorrowingEntity entity) throws Exception {
        ArrayList<BookEntity> status = bookRepository.getBookStatus(entity.getBook_id());

        if(!status.isEmpty()){
            if (status.get(0).getStatus() == 1) throw new Exception("The book is already borrowed" );
            status.get(0).setStatus(1);
            bookRepository.save(status.get(0));
            return borrowingRepository.save(entity);
        }else{
            throw new Exception("The book doesn't exist");
        }
    }

    public Collection<BorrowingEntity> list() {
        return borrowingRepository.findAll();
    }

    public BorrowingEntity get(long id_borrow) throws WrongIDException {
        Optional<BorrowingEntity> borrow = borrowingRepository.findById(id_borrow);
        if (!borrow.isPresent()) throw WrongIDException.builder(String.valueOf(id_borrow), BorrowingEntity.class);
        return borrow.get();
    }
}
