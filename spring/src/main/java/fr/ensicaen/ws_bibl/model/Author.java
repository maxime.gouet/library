package fr.ensicaen.ws_bibl.model;

import java.util.Date;

public class Author {
    private String firstname;
    private String lastname;
    private String[] books;

    public Author(String firstname, String lastname, String[] books) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.books = books;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String[] getBooks() {
        return books;
    }
}
