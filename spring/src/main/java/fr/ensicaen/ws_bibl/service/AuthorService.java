package fr.ensicaen.ws_bibl.service;

import fr.ensicaen.ws_bibl.entity.AuthorEntity;
import fr.ensicaen.ws_bibl.exception.AlreadyInDatabaseException;
import fr.ensicaen.ws_bibl.exception.WrongIDException;
import fr.ensicaen.ws_bibl.repository.AuthorRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Service
public class AuthorService {
    private final AuthorRepository authorRepository;
    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public AuthorEntity add(AuthorEntity author) throws AlreadyInDatabaseException {
        Optional<AuthorEntity> exists = authorRepository.findBylastandfirstName(author.getFirstname(),author.getLastname());
        if (exists.isPresent()) throw AlreadyInDatabaseException.builder("same_name", "name="+author.getFirstname());
        return authorRepository.save(author);
    }

    public Collection<AuthorEntity> list() {
        return authorRepository.findAll();
    }

    public AuthorEntity get(String id_author) throws WrongIDException {
        Optional<AuthorEntity> person = authorRepository.findById(UUID.fromString(id_author));
        if (!person.isPresent()) throw WrongIDException.builder(String.valueOf(id_author), AuthorEntity.class);
        return person.get();
    }

    public Collection<AuthorEntity> getByFirstandLastName(@NonNull String firstname,@NonNull String lastname) {
        return authorRepository.searchBylastandfirstName(firstname,lastname);
    }
}
