package fr.ensicaen.ws_bibl.entity;

import com.sun.istack.NotNull;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity(name="Author")
@Table(name="Author")

public class AuthorEntity {
    public AuthorEntity() {}
    public AuthorEntity(String firstname, String lastname){
        this.firstname = firstname;
        this.lastname = lastname;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UUID")
    @GenericGenerator(name="UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(updatable = false, nullable = false, length = 36)
    private UUID id;

    @NaturalId(mutable = false)
    @Column(unique = false, nullable = false, length = 64)
    @NotNull
    private String firstname;

    @NaturalId(mutable = false)
    @Column(unique = false, nullable = false, length = 64)
    @NotNull
    private String lastname;

    @ManyToMany(mappedBy = "authors")
    private Set<BookEntity> books;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "author_book",
            joinColumns = @JoinColumn(name = "author_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "book_id", referencedColumnName = "id")
    )

    public UUID getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }
}
