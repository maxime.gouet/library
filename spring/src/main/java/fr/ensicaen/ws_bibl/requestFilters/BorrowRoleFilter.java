package fr.ensicaen.ws_bibl.requestFilters;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;

public class BorrowRoleFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authorization = request.getHeader("Authorization");

        if (authorization == null) {
            response.sendError(403, "Le header HTTP Authorization est requis");
            return;
        }

        try {
            byte[] decodedBytes = Base64.getDecoder().decode(authorization);
            String role = new String(decodedBytes);
            role = role.split(":", 2)[1].toLowerCase();

            if (!role.equals("administrator_role") && !role.equals("contributor_role") && !role.equals("borrow_role")) {
                response.sendError(403, "Désolé, vous n'avez pas les droits pour effectuer cette action");
                return;
            }
        } catch (IllegalArgumentException e) {
            response.sendError(400, "Le header HTTP Authorization est incorrect");
            return;
        }

        filterChain.doFilter(request, response);
    }
}
