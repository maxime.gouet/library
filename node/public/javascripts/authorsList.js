const url = "http://localhost:8080/authors"

axios.get(url)
    .then((response) => {
        if ('data' in response) {
            let tbody = document.getElementsByTagName("tbody")[0]
            response.data.forEach((author) => {
                let toInsert = `
                    <td>${author.id}</td>
                    <td>${author.lastname}</td>
                    <td>${author.firstname}</td>`
                let tr = document.createElement("tr")
                tr.innerHTML = toInsert
                tbody.appendChild(tr);
            })
        }
    })
    .catch((err) => {
        document.getElementById("error").innerHTML = err
    })