'use strict'

const sqlite = require('sqlite3').verbose()
const hash = require('../utils/hash')

const roles = [{id: 1, role: 'CONSULT_ROLE'}, {id: 2, role: 'BORROW_ROLE'}, {id: 3, role: 'CONTRIBUTOR_ROLE'}, {id: 4, role: 'ADMINISTRATOR_ROLE'}]
const admin_user = {id: 1, name: 'admin', password: process.env['BIBLIO_ADMIN_PASSWORD'], role: 4}

class DAO {
    constructor() {
        this.db = new sqlite.Database('database/bibliotheque.db', sqlite.OPEN_READWRITE | sqlite.OPEN_CREATE, (err) => {
            if (err) {
                console.error(err.message)
            }
            console.log('Connected to the bibliotheque database.')
            console.log('Trying to create tables...')
            this.createTables()
                .then(() => console.log('Tables created successfully'))
                .catch((err) => console.log('Error while creating tables: ' + err))
        })
    }

    deleteUserByID(id) {
        const delete_sql = `
        DELETE FROM user 
        WHERE id_user = (?)`

        return this.run(delete_sql, [id])
    }

    insertUser(name, password, role) {
        const insert_sql = `
        INSERT INTO user (name, password, id_role)
        VALUES (?, ?, ?)`

        return this.run(insert_sql, [name, password, role])
    }

    getUserByID(id) {
        const get_sql = `
            SELECT id_user, name, password, role, id_role FROM user
            JOIN role USING(id_role)
            WHERE id_user = (?)`

        return this.get(get_sql, [id])
    }

    getUserByName(name) {
        const get_sql = `
            SELECT id_user, name, password, role, id_role FROM user
            JOIN role USING(id_role)
            WHERE name = (?)`

        return this.get(get_sql, [name])
    }

    getAllUsers(role) {
        const get_sql = `
            SELECT id_user, name, password, role, id_role FROM user
            JOIN role USING(id_role)
            WHERE id_role <= (?)`

        return this.all(get_sql, [role])
    }

    getAllRoles() {
        const get_sql = `SELECT * FROM role`

        return this.all(get_sql)
    }

    createTables() {
        const role_sql = `
            CREATE TABLE IF NOT EXISTS role (
            id_role INTEGER PRIMARY KEY AUTOINCREMENT,
            role TEXT)`

        const role_insert_sql = `
            INSERT INTO role (id_role, role) VALUES (?, ?)
            ON CONFLICT(id_role) DO UPDATE SET role = role` // This line prevent for duplicates

        const user_sql = `
            CREATE TABLE IF NOT EXISTS user (
            id_user INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT UNIQUE,
            password TEXT,
            id_role INTEGER,
            CONSTRAINT user_fk_role FOREIGN KEY (id_role)
                REFERENCES role(id_role) ON UPDATE CASCADE ON DELETE CASCADE)`

        const user_insert_sql = `
        INSERT INTO user (id_user, name, password, id_role) VALUES (?, ?, ?, ?)
        ON CONFLICT(id_user) DO UPDATE SET password = password, id_role = id_role`

        return new Promise(async (resolve, reject) => {
            try {
                await this.run(role_sql)
                await this.run(user_sql)
                for (let i = 0; i < roles.length; i++) {
                    await this.run(role_insert_sql, [roles[i].id, roles[i].role])
                }
                if (admin_user.password === undefined)
                    reject("Admin password isn't set")
                const password = hash.encrypt(admin_user.password)
                await this.run(user_insert_sql, [admin_user.id, admin_user.name, password, admin_user.role])
                resolve('OK')
            } catch (err) {
                reject(err)
            }
        })
    }

    run(sql, params = []) {
        return new Promise((resolve, reject) => {
            this.db.run(sql, params, function (err) {
                if (err) {
                    reject(err)
                }
                resolve({ id: this.lastID })
            })
        })
    }

    get(sql, params = []) {
        return new Promise((resolve, reject) => {
            this.db.get(sql, params, (err, result) => {
                if (err) {
                    reject(err)
                }
                resolve(result)
            })
        })
    }

    all(sql, params = []) {
        return new Promise((resolve, reject) => {
            this.db.all(sql, params, (err, rows) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(rows)
                }
            })
        })
    }
}

module.exports = DAO