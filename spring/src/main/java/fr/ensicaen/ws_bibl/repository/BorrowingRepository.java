package fr.ensicaen.ws_bibl.repository;

import fr.ensicaen.ws_bibl.entity.BorrowingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;

public interface BorrowingRepository extends JpaRepository<BorrowingEntity, Long> {

    @Query(value="SELECT b FROM Borrowing b WHERE lower(b.id) = lower(:bookid)")
    ArrayList<BorrowingEntity> getBookStatus(@Param("bookid") long bookid);
}
