'use strict'

const express = require('express');
const router = express.Router();
const process = require('../api/users')

router.use(function (req, res, next) {
    let auth = req.headers.authorization
    if (auth === undefined) {
        res.set('WWW-Authenticate', 'Basic realm="User Visible Realm", charset="UTF-8"')
        res.status(401).json({ message: 'Missing Authorization Header' });
    } else {
        let encoded = auth.split(' ')[1]
        let credentials = Buffer.from(encoded, 'base64').toString().split(':')
        process.verifyIdentity(credentials[0], credentials[1])
            .then((user) => {
                // Here I create in the locals an object user that i can use later in sub middlewares
                res.locals.user = {name: credentials[0], role: user.id_role, roleName: user.role}
                next()
            })
            .catch((err) => {
                if (err === 'Wrong password') {
                    res.status(401).send({message: err})
                } else {
                    res.status(404).send({message: err})
                }
            })
    }
})

router.get('/', (req, res) => {
    process.getAllUsers(res.locals.user.role)
        .then((response) => {
            res.status(200).send(response)
        })
        .catch((err) => {
            res.status(500).send({ message: err })
        })
})

router.get('/roles', (req, res) => {
    process.getRoles()
        .then((response) => {
            res.status(200).send(response)
        })
        .catch((err) => {
            res.status(500).send({ message: err })
        })
})

router.get('/:id', (req, res) => {
    process.getUserByID(req.params.id, res.locals.user.role)
        .then((response) => res.status(200).send(response))
        .catch((err) => res.status(500).send({ message: err }))
})

router.post('/', (req, res, next) => {
    process.add(req.body, res.locals.user.role)
        .then(() => {
            res.status(201).send('Utilisateur ajouté avec succès')
        })
        .catch((err) => {
            res.status(400).send({message: err})
        })
})

router.delete('/:id', (req, res) => {
    process.deleteByID(req.params.id, res.locals.user.role)
        .then((response) => {
            res.status(200).send({ message: "OK" })
        })
        .catch((err) => {
            res.status(500).send({message: err})
        })
})

module.exports = router;
