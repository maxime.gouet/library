package fr.ensicaen.ws_bibl.controller;

import fr.ensicaen.ws_bibl.entity.BorrowingEntity;
import fr.ensicaen.ws_bibl.exception.AlreadyInDatabaseException;
import fr.ensicaen.ws_bibl.service.BorrowingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("borrowing")
public class BorrowingController {
    private final BorrowingService borrowingService;
    public BorrowingController(BorrowingService service) {
        this.borrowingService = service;
    }

    @GetMapping(value = "/", produces = "application/json")
    public Collection<BorrowingEntity> get_all() {
            return borrowingService.list();
    }

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> add(@RequestBody BorrowingEntity entity ) throws AlreadyInDatabaseException {

        try {
            BorrowingEntity newBorrowing = borrowingService.add(entity);
            return ResponseEntity.status(HttpStatus.CREATED).body(newBorrowing);
        }catch(Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body("Book is already borrowed");
    }
}
