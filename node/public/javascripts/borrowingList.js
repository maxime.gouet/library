const borrowing_url = "http://localhost:8080/borrowings/"
const users_url = "http://localhost:8080/users/"
const books_url = "http://localhost:8080/books/"

axios.get(borrowing_url)
    .then((response) => {
        if ('data' in response) {
            let tbody = document.getElementsByTagName("tbody")[0]
            response.data.forEach(async (borrow) => {
                try {
                    let user = await axios.get(users_url + borrow.id_user)
                    let book = await axios.get(books_url + borrow.book_id)

                    let toInsert = `
                        <td>${borrow.id}</td>
                        <td>${borrow.borrow_date}</td>
                        <td>${borrow.return_date}</td>
                        <td>${book.data.name}</td>
                        <td>${user.data.name}</td>`
                    let tr = document.createElement("tr")
                    tr.innerHTML = toInsert
                    tbody.appendChild(tr);
                } catch(err) {
                    document.getElementById("error").innerHTML = err
                }
            })
        }
    })
    .catch((err) => {
        document.getElementById("error").innerHTML = err
    })