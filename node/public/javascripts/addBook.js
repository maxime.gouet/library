const publisher_url = "http://localhost:8080/publishers/"
const author_url = "http://localhost:8080/authors/"
const book_url = "http://localhost:8080/books/"

axios.get(publisher_url)
    .then((response) => {
        if ('data' in response) {
            let select = document.getElementById("publisher")
            response.data.forEach((publisher) => {
                let option = document.createElement("option")
                option.innerHTML = publisher.name
                option.value = publisher.id
                select.appendChild(option)
            })
        }
    })
    .catch((err) => {
        document.getElementById("error").innerHTML = err
    })

axios.get(author_url)
    .then((response) => {
        if ('data' in response) {
            let select = document.getElementById("author")
            response.data.forEach((author) => {
                let option = document.createElement("option")
                option.innerHTML = author.firstname + " " + author.lastname
                option.value = author.id
                select.appendChild(option)
            })
        }
    })
    .catch((err) => {
        document.getElementById("error").innerHTML = err
    })

document.getElementsByTagName("button")[0].onclick = function() {
    let name = document.getElementById("name").value
    let date = document.getElementById("date").value
    let publisher = document.getElementById("publisher").value
    let author = document.getElementById("author").value

    axios({
        method: 'post',
        url: book_url,
        data: {
            name: name,
            publish_date: date,
            status: 0,
            publisher_id: publisher,
            author_id: [author]
        }
    })
        .then(() => {
            document.getElementById("ok").innerHTML = "Livre ajouté avec succès"
            document.getElementById("error").innerHTML = ""
        })
        .catch((err) => {
            document.getElementById("ok").innerHTML = ""
            document.getElementById("error").innerHTML = err
        })
}
