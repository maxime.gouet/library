package fr.ensicaen.ws_bibl.configurations;

import fr.ensicaen.ws_bibl.requestFilters.BorrowRoleFilter;
import fr.ensicaen.ws_bibl.requestFilters.ContributorRoleFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class AuthorizationProvider extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.antMatcher("/borrowing**").addFilterBefore(new BorrowRoleFilter(), BasicAuthenticationFilter.class);

        http.requestMatchers().antMatchers("/publisher**", "/book**", "/author**")
                .and()
                .addFilterBefore(new ContributorRoleFilter(), BasicAuthenticationFilter.class);
    }
}
