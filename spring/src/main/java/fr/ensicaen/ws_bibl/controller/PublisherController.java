package fr.ensicaen.ws_bibl.controller;

import fr.ensicaen.ws_bibl.entity.PublisherEntity;
import fr.ensicaen.ws_bibl.exception.WrongIDException;
import fr.ensicaen.ws_bibl.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("publisher")
public class PublisherController {
    @Autowired
    private PublisherService publisherService;

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<PublisherEntity> getAll(@RequestParam(required = false) String bydesc) {
//        if (bydesc != null) {
//            return publisherService.findByName(bydesc);
//        }
        return publisherService.list();
    }

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PublisherEntity> add(@RequestBody PublisherEntity publisher) {
        PublisherEntity newpublisher = publisherService.add(publisher.getName());
        return ResponseEntity.status(HttpStatus.CREATED).body(newpublisher);
    }

    @PostMapping("/addByForm")
    public ResponseEntity<PublisherEntity> addByForm(@RequestParam(required = true) String name) {
        PublisherEntity newpublisher = publisherService.add(name);
        return ResponseEntity.status(HttpStatus.CREATED).body(newpublisher);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getById(@PathVariable String id) throws WrongIDException {
        Optional<PublisherEntity> publisher = publisherService.getById(id);
        if (publisher.isEmpty())
            return notfound(id);
        else
            return ResponseEntity.ok(publisher.get());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteById(@PathVariable String id) throws WrongIDException {
        Optional<PublisherEntity> publisher = publisherService.getById(id);
        if (publisher.isEmpty())
            return notfound(id);
        else {
            publisherService.deleteById(id);
            return ResponseEntity.ok(publisher);
        }
    }

//    @PutMapping("/")
//    public void update(@RequestBody PublisherEntity publisher) throws WrongIDException {
//        if (!publisherService.update(publisher))
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
//    }

    private ResponseEntity<Object> notfound(String id) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("<html><body>cannot found PublisherEntity with id=" + id + "</body></html>");
    }
}
