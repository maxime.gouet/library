package fr.ensicaen.ws_bibl.service;

import fr.ensicaen.ws_bibl.entity.BookEntity;
import fr.ensicaen.ws_bibl.exception.WrongIDException;
import fr.ensicaen.ws_bibl.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BookService {
    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public BookEntity add(BookEntity bookEntity) {
        return bookRepository.save(bookEntity);
    }

    public List<BookEntity> list() {
        return bookRepository.findAll();
    }

    public Optional<BookEntity> getById(String id) throws WrongIDException {
        try {
            return bookRepository.findById(UUID.fromString(id));
        } catch (IllegalArgumentException e) {
            throw WrongIDException.builder("Invalid UUID String", BookEntity.class);
        }
    }

    public Collection<BookEntity> getByName(String name) {
        return bookRepository.findByName(name);
    }
}
