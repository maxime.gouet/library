package fr.ensicaen.ws_bibl;

import fr.ensicaen.ws_bibl.exception.WrongIDException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

@SpringBootApplication
public class WsBiblApplication {

    @Value("${spring.application.name}")
    private String name;

    public static void main(String[] args) {
        SpringApplication.run(WsBiblApplication.class, args);
    }

    @Bean
    public ErrorAttributes errorAttributes() {
        return new DefaultErrorAttributes() {
            @Override
            public Map<String, Object> getErrorAttributes(WebRequest requestAttributes,
                                                          ErrorAttributeOptions options) {
                Map<String, Object> errorAttributes = super.getErrorAttributes(requestAttributes, options);
                Throwable error = getError(requestAttributes);
                if (error instanceof WrongIDException) {
                    errorAttributes.put("id", ((WrongIDException) error).getId());
                    errorAttributes.put("entity", ((WrongIDException) error).getEntity());
                    errorAttributes.put("table", ((WrongIDException) error).getTable());
                }
//                if (error instanceof AlreadyInDatabaseException) {
//                    if (!((AlreadyInDatabaseException)error).getAdditionalInformation().isEmpty())
//                        errorAttributes.put("additionalInformation", ((AlreadyInDatabaseException)error).getAdditionalInformation());
//                }
                if (error != null && error.getLocalizedMessage() != null)
                    errorAttributes.put("localizedMessage", error.getLocalizedMessage());
                return errorAttributes;
            }
        };
    }
}
