const author_url = "http://localhost:8080/authors"

document.getElementsByTagName("button")[0].onclick = function() {
    let firstname = document.getElementById("firstname").value
    let lastname = document.getElementById("lastname").value

    axios({
        method: 'post',
        url: author_url,
        data: {
            firstname: firstname,
            lastname: lastname
        }
    })
        .then(() => {
            document.getElementById("ok").innerHTML = "Auteur ajouté avec succès"
            document.getElementById("error").innerHTML = ""
        })
        .catch((err) => {
            document.getElementById("ok").innerHTML = ""
            document.getElementById("error").innerHTML = err
        })
}