package fr.ensicaen.ws_bibl.service;

import fr.ensicaen.ws_bibl.entity.PublisherEntity;
import fr.ensicaen.ws_bibl.exception.WrongIDException;
import fr.ensicaen.ws_bibl.model.Publisher;
import fr.ensicaen.ws_bibl.repository.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Service
public class PublisherService {
    @Autowired
    private PublisherRepository publisherRepository;

    public Collection<PublisherEntity> list() {
        return publisherRepository.findAll();
    }

    public Collection<PublisherEntity> findByName(String desc) {
        return publisherRepository.findByName(desc);
    }

    @Transactional
    public PublisherEntity add(String name) {
        PublisherEntity newPublisher = new PublisherEntity(name);
        publisherRepository.save(newPublisher);
        return newPublisher;
    }

    public Optional<PublisherEntity> getById(String id) throws WrongIDException {
        try {
            UUID uuid = UUID.fromString(id);
            return publisherRepository.findById(uuid);
        } catch (IllegalArgumentException e) {
            throw WrongIDException.builder("Invalid UUID String", PublisherEntity.class);
        }
    }

    public void deleteById(String id) {
        publisherRepository.deleteById(UUID.fromString(id));
    }

//    public boolean update(Publisher publisher) throws WrongIDException {
//        Optional<PublisherEntity> t = getById(publisher.getId());
//        if (t.isEmpty()) return false;
//        t.get().setName(publisher.getName());
//        publisherRepository.save(t.get());
//        return true;
//    }
}
