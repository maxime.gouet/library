const url = "http://localhost:8080/users/"

axios.get(url)
    .then((response) => {
        if ('data' in response) {
            let tbody = document.getElementsByTagName("tbody")[0]
            response.data.forEach((user) => {
                let toInsert = `
                    <td>${user.id_user}</td>
                    <td>${user.name}</td>
                    <td>${user.role}</td>
                    <td><button id="btn_${user.id_user}" onclick="supprimer(${user.id_user})">Supprimer</button></td>`
                let tr = document.createElement("tr")
                tr.innerHTML = toInsert
                tbody.appendChild(tr);
                if (user.role === "ADMINISTRATOR_ROLE") {
                    document.getElementById('btn_'+user.id_user).disabled = true
                }
            })
        }
    })
    .catch((err) => {
        document.getElementById("error").innerHTML = err
    })

function supprimer(id) {
    axios({
        method: 'delete',
        url: url + "/"+id,
    })
        .then(() => {
            document.location.reload()
        })
        .catch((err) => {
            document.getElementById("error").innerHTML = err
        })
}