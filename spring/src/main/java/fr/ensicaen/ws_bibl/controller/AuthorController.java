package fr.ensicaen.ws_bibl.controller;

import fr.ensicaen.ws_bibl.entity.AuthorEntity;
import fr.ensicaen.ws_bibl.exception.AlreadyInDatabaseException;
import fr.ensicaen.ws_bibl.model.Author;
import fr.ensicaen.ws_bibl.service.AuthorService;
import fr.ensicaen.ws_bibl.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("author")
public class AuthorController {
    private final AuthorService authorService;
    private final BookService bookService;

    public AuthorController(AuthorService service, BookService bookService) {
        this.authorService = service;
        this.bookService = bookService;
    }

    @GetMapping(value = "/", produces = "application/json")
    public Collection<AuthorEntity> index(@RequestParam(required = false) String firstname,@RequestParam(required = false) String lastname) {
        if (firstname != null && lastname != null ) {
            return authorService.getByFirstandLastName(firstname,lastname);
        } else
            return authorService.list();
    }

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> add(@RequestBody Author author) throws AlreadyInDatabaseException {
        AuthorEntity authorEntity = new AuthorEntity(
            author.getFirstname(),
            author.getLastname()
        );
        AuthorEntity newAuthor = authorService.add(authorEntity);
        return ResponseEntity.status(HttpStatus.CREATED).body(newAuthor);
    }

    /*@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public AuthorEntity getById(@PathVariable String id) throws WrongIDException {
        return authorService.get(id);
    }*/
}
