'use strict'

const axios = require('axios')
const express = require('express')
const router = express.Router()
const process = require('../api/users')

const base_url = "http://localhost:8081/"
const author_url = base_url + "author/"
const publisher_url = base_url + "publisher/"
const book_url = base_url + "book/"
const borrowing_url = base_url + "borrowing/"

router.use(function (req, res, next) {
    let auth = req.headers.authorization
    if (auth === undefined) {
        res.set('WWW-Authenticate', 'Basic realm="User Visible Realm", charset="UTF-8"')
        res.status(401).json({ message: 'Missing Authorization Header' });
    } else {
        let encoded = auth.split(' ')[1]
        let credentials = Buffer.from(encoded, 'base64').toString().split(':')
        process.verifyIdentity(credentials[0], credentials[1])
            .then((user) => {
                // Here I create in the locals an object user that i can use later in sub middlewares
                res.locals.user = {idUser: user.id_user, role: user.role}
                next()
            })
            .catch((err) => {
                if (err === 'Wrong password') {
                    res.status(401).send({message: err})
                } else {
                    res.status(404).send({message: err})
                }
            })
    }
})

router.get('/borrowings', (req, res) => {
    let auth = new Buffer(res.locals.user.idUser + ":" + res.locals.user.role)
    axios({
        method: 'get',
        url: borrowing_url,
        headers: {
            Authorization: auth.toString('base64')
        },
    })
        .then((response) => {
            if ('data' in response) {
                res.status(200).send(response.data)
            }
        })
        .catch((err) => {
            res.status(500).send({ message: err })
        })
})

router.post('/borrowings', (req, res) => {
    let auth = new Buffer(res.locals.user.idUser + ":" + res.locals.user.role)
    axios({
        method: 'post',
        url: borrowing_url,
        headers: {
            Authorization: auth.toString('base64')
        },
        data: req.body
    })
        .then(() => {
            res.status(200).send({ message: "OK" })
        })
        .catch((err) => {
            res.status(500).send({ message: err })
        })
})

router.post('/books', (req, res) => {
    let auth = new Buffer(res.locals.user.idUser + ":" + res.locals.user.role)
    axios({
        method: 'post',
        url: book_url,
        headers: {
            Authorization: auth.toString('base64')
        },
        data: req.body
    })
        .then(() => {
            res.status(200).send({ message: "OK" })
        })
        .catch((err) => {
            res.status(500).send({ message: err })
        })
})

router.get('/books', (req, res) => {
    let auth = new Buffer(res.locals.user.idUser + ":" + res.locals.user.role)
    axios({
        method: 'get',
        url: book_url,
        headers: {
            Authorization: auth.toString('base64')
        }
    })
        .then((response) => {
            if ('data' in response) {
                res.status(200).send(response.data)
            }
        })
        .catch((err) => res.status(500).send({ message: err }))
})

router.get('/books/:id', (req, res) => {
    let auth = new Buffer(res.locals.user.idUser + ":" + res.locals.user.role)
    axios({
        method: 'get',
        url: book_url + req.params.id,
        headers: {
            Authorization: auth.toString('base64')
        }
    })
        .then((response) => {
            if ('data' in response) {
                res.status(200).send(response.data)
            }
        })
        .catch((err) => res.status(500).send({ message: err }))
})

router.get('/publishers', (req, res) => {
    let auth = new Buffer(res.locals.user.idUser + ":" + res.locals.user.role)
    axios({
        method: 'get',
        url: publisher_url,
        headers: {
            Authorization: auth.toString('base64')
        }
    })
        .then((response) => {
            if ('data' in response) {
                res.status(200).send(response.data)
            }
        })
        .catch((err) => res.status(500).send({ message: err }))
})

router.get('/publishers/:id', (req, res) => {
    let auth = new Buffer(res.locals.user.idUser + ":" + res.locals.user.role)
    axios({
        method: 'get',
        url: publisher_url + "/"+req.params.id,
        headers: {
            Authorization: auth.toString('base64')
        }
    })
        .then((response) => {
            if ('data' in response) {
                res.status(200).send(response.data)
            }
        })
        .catch((err) => res.status(500).send({ message: err }))
})

router.post('/publishers', (req , res) => {
    let auth = new Buffer(res.locals.user.idUser + ":" + res.locals.user.role)
    axios({
        method: 'post',
        url: publisher_url,
        headers: {
            Authorization: auth.toString('base64')
        },
        data: req.body
    })
        .then(() => {
            res.status(200).send({ message: "OK" })
        })
        .catch((err) => {
            res.status(500).send({ message: err })
        })
})

router.get('/authors', (req, res) => {
    let auth = new Buffer(res.locals.user.idUser + ":" + res.locals.user.role)
    axios({
        method: 'get',
        url: author_url,
        headers: {
            Authorization: auth.toString('base64')
        }
    })
        .then((response) => {
            if ('data' in response) {
                res.status(200).send(response.data)
            }
        })
        .catch((err) => res.status(500).send({ message: err }))
})

router.post('/authors', (req, res) => {
    let auth = new Buffer(res.locals.user.idUser + ":" + res.locals.user.role)
    axios({
        method: 'post',
        url: author_url,
        headers: {
            Authorization: auth.toString('base64')
        },
        data: req.body
    })
        .then(() => res.status(200).send({ message: "OK" }))
        .catch((err) => res.status(500).send({ message: err }))
})

module.exports = router