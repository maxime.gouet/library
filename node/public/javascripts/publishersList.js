const url = "http://localhost:8080/publishers"

axios.get(url)
    .then((response) => {
        if ('data' in response) {
            let tbody = document.getElementsByTagName("tbody")[0]
            response.data.forEach((publisher) => {
                let toInsert = `
                    <td>${publisher.id}</td>
                    <td>${publisher.name}</td>`
                let tr = document.createElement("tr")
                tr.innerHTML = toInsert
                tbody.appendChild(tr);
            })
        }
    })
    .catch((err) => {
        document.getElementById("error").innerHTML = err
    })