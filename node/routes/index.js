const express = require('express')
const router = express.Router()

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Gestionnaire de bibliothèque' })
})

router.get('/addUser', function(req, res) {
  res.render('addUser', { title: 'Ajouter un utilisateur' })
})

router.get('/addAuthor', function(req, res) {
  res.render('addAuthor', { title: 'Ajouter un auteur' })
})

router.get('/usersList', function(req, res) {
  res.render('usersList', { title: 'Liste des utilisateurs' })
})

router.get('/authorsList', function(req, res) {
  res.render('authorsList', { title: 'Liste des auteurs' })
})

router.get('/addPublisher', function(req, res) {
  res.render('addPublisher', { title: 'Ajouter un éditeur' })
})

router.get('/publishersList', function(req, res) {
  res.render('publishersList', { title: 'Liste des éditeurs' })
})

router.get('/addBook', function(req, res) {
  res.render('addBook', { title: 'Ajouter un livre' })
})

router.get('/booksList', function(req, res) {
  res.render('booksList', { title: 'Liste des livres' })
})

router.get('/makeBorrow', function(req, res) {
  res.render('makeBorrow', { title: 'Ajouter un emprunt' })
})

router.get('/borrowingList', function(req, res) {
  res.render('borrowingList', { title: 'Liste des emprunts' })
})

module.exports = router
