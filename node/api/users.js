'use strict'

const UsersDAO = require('./dao')
const hash = require('../utils/hash')

const dao = new UsersDAO()

const add = function (body, role) {
    return new Promise((resolve, reject) => {
        if (body.name === "") {
            reject("No name")
            return
        }
        if (body.password === "") {
            reject("No password")
            return
        }
        if (body.role === undefined) {
            reject("No role")
            return
        }

        if (role <= body.role) {
            reject(`You are not allowed to create an user of role '${body.role}' with the role '${role}'`)
            return
        }

        const password = hash.encrypt(body.password)

        dao.insertUser(body.name, password, body.role)
            .then((res) => resolve(res))
            .catch((err) => reject(err))
    })
}

const deleteByID = function(id, role) {
    return new Promise((resolve, reject) => {
        dao.getUserByID(id)
            .then((user) => {
                if (user.id_role >= role)
                    reject(`You are not allowed to delete an user of role '${user.id_role}' with the role '${role}'`)
                dao.deleteUserByID(id, role)
                    .then((res) => resolve(res))
                    .catch((err) => reject(err))
            })
            .catch(() => reject('Unknown user'))
    })
}

const getAllUsers = function (role) {
    return dao.getAllUsers(role)
}

const getUserByID = function (id) {
    return dao.getUserByID(id)
}

const verifyIdentity = function(name, password) {
    return new Promise((resolve, reject) => {
        dao.getUserByName(name)
            .then((user) => {
                if (hash.decrypt(user.password) === password) {
                    resolve(user)
                }
                reject('Wrong password')
            })
            .catch(() => reject('Unknown user'))
    })
}

const getRoles = function() {
    return dao.getAllRoles()
}

const getUserByName = function (name) {
    return dao.getUserByName(name)
}

exports.getUserByName = getUserByName
exports.add = add
exports.deleteByID = deleteByID
exports.verifyIdentity = verifyIdentity
exports.getAllUsers = getAllUsers
exports.getUserByID = getUserByID
exports.getRoles = getRoles