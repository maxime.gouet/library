package fr.ensicaen.ws_bibl.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity(name="Borrowing")
@Table(name="Borrowing")

public class BorrowingEntity {
    public BorrowingEntity() {}
    public BorrowingEntity(int borrow_duration, UUID book_id,String id_user){
        this.borrow_date = new Date();
        this.borrow_duration = borrow_duration;
        this.return_date = new Date(borrow_date.getTime() + (borrow_duration*(1000 * 60 * 60 * 24)));
        this.book_id = book_id;
        this.id_user = id_user;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UUID")
    @GenericGenerator(name="UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(updatable = false, nullable = false, length = 36)
    private UUID id;

    @NaturalId(mutable = false)
    @Column(unique = false, nullable = false, length = 64)
    private Date borrow_date = new Date();

    @NaturalId(mutable = false)
    @Column(unique = false, nullable = false, length = 64)
    private int borrow_duration;

    @NaturalId(mutable = false)
    @Column(unique = false, nullable = false, length = 64)
    private Date return_date = new Date(borrow_date.getTime() + (borrow_duration*(1000 * 60 * 60 * 24)));

    @NaturalId(mutable = false)
    @Column(unique = false, nullable = false, length = 64)
    private UUID book_id;

    @NaturalId(mutable = false)
    @Column(unique = false, nullable = false, length = 36)
    private String id_user;

    public UUID getId() {
        return id;
    }

    public Date getBorrow_date() {
        return borrow_date;
    }

    public int getBorrow_duration() {
        return borrow_duration;
    }

    public Date getReturn_date() {
        return return_date;
    }
    public String getId_user() {
        return id_user;
    }


    public UUID getBook_id() {
        return book_id;
    }
}
