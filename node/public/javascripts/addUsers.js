const users_url = "http://localhost:8080/users/"

axios.get(users_url+"roles")
    .then((response) => {
        if ('data' in response) {
            let select = document.getElementsByTagName("select")[0]
            response.data.forEach((role) => {
                let option = document.createElement("option")
                option.innerHTML = role.role
                option.value = role.id_role
                select.appendChild(option)
            })
        }
    })
    .catch((err) => {
        document.getElementById("error").innerHTML = err
    })

document.getElementsByTagName("button")[0].onclick = function() {
    let name = document.getElementById("name").value
    let pass = document.getElementById("pass").value
    let role = document.getElementById("role").value

    axios({
        method: 'post',
        url: users_url,
        data: {
            name: name,
            password: pass,
            role: role
        }
    })
        .then(() => {
            document.getElementById("ok").innerHTML = "Utilisateur ajouté avec succès"
            document.getElementById("error").innerHTML = ""
        })
        .catch((err) => {
            document.getElementById("ok").innerHTML = ""
            document.getElementById("error").innerHTML = err
        })
}