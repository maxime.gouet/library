package fr.ensicaen.ws_bibl.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.*;

@Entity(name = "Book")
@Table(name = "Book")
public class BookEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false, length = 36)
    private UUID id;

    @Column(nullable = false, length = 100)
    private String name;

    @Column(nullable = false)
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date publish_date;

    @Column(nullable = false)
    private int status;

    @Column(unique = false, nullable = false, length = 64)
    private UUID publisher_id;

    @ManyToMany
    @JoinTable(
            name = "AUTHOR_BOOK",
            joinColumns = @JoinColumn(name = "BOOK_ID"),
            inverseJoinColumns = @JoinColumn(name = "AUTHOR_ID"))
    private Set<AuthorEntity> authors;

    protected BookEntity() {
    }

    public BookEntity(String name, Date publish_date, int status,UUID publisher_id) {
        this.name = name;
        this.publish_date = publish_date;
        this.status = status;
        this.publisher_id = publisher_id;

    }

    public void setStatus(int status) {
        this.status = status;
    }

    public UUID getId() {
        return id;
    }

    public int getStatus() {
        return status;
    }

    public Date getPublish_date() {
        return publish_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getPublisher_id(){
        return publisher_id;
    }
}
