const publisher_url = "http://localhost:8080/publishers"

document.getElementsByTagName("button")[0].onclick = function() {
    let name = document.getElementById("name").value

    axios({
        method: 'post',
        url: publisher_url,
        data: {
            name: name
        }
    })
        .then(() => {
            document.getElementById("ok").innerHTML = "Auteur ajouté avec succès"
            document.getElementById("error").innerHTML = ""
        })
        .catch((err) => {
            document.getElementById("ok").innerHTML = ""
            document.getElementById("error").innerHTML = err
        })
}