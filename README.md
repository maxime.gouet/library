# Users service

You first need to define the admin password and the secret key used to encrypt the password using environment variable:
- BIBLIO_ADMIN_PASSWORD=<pass> (default: ``Test``)
- BIBLIO_SECRET=<256bits_secret> (default: ``w9z$C&F)J@NcRfTjWnZr4u7x!A%D*G-K``)

## Start the server (outside / inside Intellij)

This user will be created :
````json
{
  "id": 1,
  "name": "admin",
  "password": "<hashed_password>",
  "role": 4
}
````

Start the server with the following command :
`````shell script
cd node
npm install
npm run start
`````

# Books service

You first need to have a Java 11 and your JAVA_HOME set to his location.

## Start the server (inside Intellij)
Open the project from the root ``bibliotheque/spring``
Run it with gradle.

## Start the server (outside of Intellij)

Start the server with the following command :
`````shell script
cd spring
./gradlew bootRun
`````

# Databases provided
The databases provided aren't empty. There is some users, books, authors, borrows and publishers.
The default admin user is : `admin` : `Test`.

# How to connect
In order to keep the front simple, we are using the provided basic auth dialogue from the browser (Brave/Firefox/Chrome) to log in.
So if you want to log out, you need to restart your browser and the dialogue will show again.

# How works the dialogue between front / backend
In order to retrieve, for example, the books list, the front is requesting the back of node at the endpoints ``/books``.
The back will retrive the authorization header, verify the user identity and if it's correct, it will send the request to the spring back with the header:
``Authorization: <user_id>:<user_role>`` encoded in base64. Thanks to that, the spring back will know which user is requesting and what is his role.