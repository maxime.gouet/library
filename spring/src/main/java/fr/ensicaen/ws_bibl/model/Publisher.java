package fr.ensicaen.ws_bibl.model;

import java.util.UUID;

public class Publisher {

    private String id;
    private String name;

    private Publisher() {}
    public Publisher(String name) {
        this.name = name;
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String description) {
        this.name = description;
    }
}
