package fr.ensicaen.ws_bibl.repository;

import fr.ensicaen.ws_bibl.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, UUID> {
    @Query("SELECT b FROM Book b WHERE lower(b.name) like lower(concat('%', :partial_desc, '%'))")
    Collection<BookEntity> findByName(@Param("partial_desc") String name);

    @Query(value="SELECT b FROM Book b WHERE lower(b.id) = lower(:bookid)")
    ArrayList<BookEntity> getBookStatus(@Param("bookid") UUID bookid);
}
