package fr.ensicaen.ws_bibl.repository;

import fr.ensicaen.ws_bibl.entity.PublisherEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.UUID;

@Repository
public interface PublisherRepository extends JpaRepository<PublisherEntity, UUID> {
    @Query(value="SELECT p FROM Publisher p WHERE lower(p.name) like lower(concat('%', :partial_desc, '%'))")
    Collection<PublisherEntity> findByName(@Param("partial_desc") String name);
}
