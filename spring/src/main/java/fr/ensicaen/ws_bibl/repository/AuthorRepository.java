package fr.ensicaen.ws_bibl.repository;

import fr.ensicaen.ws_bibl.entity.AuthorEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface AuthorRepository extends JpaRepository<AuthorEntity, UUID> {

    @Query(value="SELECT a FROM Author a WHERE lower(a.firstname) = lower(:firstname) and lower(a.lastname) = lower(:lastname)")
    Optional<AuthorEntity> findBylastandfirstName(@Param("firstname") String firstname,@Param("lastname") String lastname);

    @Query(value="SELECT a FROM Author a WHERE lower(a.firstname) = lower(:firstname) and lower(a.lastname) = lower(:lastname)")
    Collection<AuthorEntity> searchBylastandfirstName(@Param("firstname") String firstname,@Param("lastname") String lastname);
}
